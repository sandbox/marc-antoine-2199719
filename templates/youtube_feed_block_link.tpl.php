<?php

/**
 * @file
 * Theme file to display youtubechannel.
 */
if ($vars['content']) :
?>

  <div id="youtube_feed_block_videosWrapper" >
    <ul>
      <?php foreach ($vars['content'] as $value) : ?>
        <li class="<?php echo $value['video_classes']; ?>">
            <?php echo $value['video_link']; ?>
        </li>
      <?php endforeach; ?>
    </ul>
    <div class='clear'></div>
  </div>
<?php endif; ?>
