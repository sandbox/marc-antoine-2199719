/**
 * @file
 *
 * This file makes sure that every video list element has the same height.
 *
 * Using jquery, we detect the highest li element within our video wrapper, 
 * and adjust our height accordingly. Normally, this file should only be
 * included if the videos' titles are shown.
 */

jQuery(function($){
  $(document).ready(function() {
    setEqualHeight($("#youtube_feed_block_videosWrapper ul li"));
  });

  function setEqualHeight(columns){
    var tallestcolumn = 0;
    columns.each(function(){
      currentHeight = $(this).height();
      if(currentHeight > tallestcolumn){
        tallestcolumn  = currentHeight;
      }
    });
    columns.height(tallestcolumn);
  }
});
