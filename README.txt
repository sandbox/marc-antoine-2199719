
-- SUMMARY --

The Youtube Feed Block module generates a block displaying a list of the most
recent youtube videos from a specific youtube channel.

For a full description of the module, visit the project page:
  https://drupal.org/sandbox/marc-antoine/2199719

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2199719


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Once the installation is done, you might want to head towards the module's
  configuration (/config/media/youtube_feed_block).


-- CONFIGURATION --

* The configuration page (/config/media/youtube_feed_block) currently includes:

  - The Channel's name
  
    This is the name used to fetch youtube's videos. 
    (usually right after the user e.g. youtube.com/user/YourChannelName)
  
  - The number of videos to display
    
    If there isn't enough videos, the module will simply pick everything it 
    finds.
  
  - The video titles display 
  
    This option displays the video's title below the video's thumbnail.
  
  - The video titles max length
  
    This option will automatically crop the video's title with the specified
    length and add a '...' to it.
  
  - The thumbnail's width
  
    This option is used to specify the width used for the video's thumbnail. If
    this value is smaller than 120px, the thumbnail used will be 1.jpg
    (this is because the 0.jpg thumbnail has a higher resolution)
  
  - The thumbnail's height (will crop the image)
  
    This option will crop the height of the thumbnail, usually used to prevent
    black bars to show, depending of the video's aspect ratio.

-- CONTACT --

Current maintainers:
* Marc-Antoine tremblay (marc-antoine) - https://drupal.org/user/1602302

This project has been sponsored by:
* Mobile Programmation
  Websites and mobile apps creators, Mobile Programmation offers their expertise
  using multiple technologies, including Drupal!
  Visit http://www.mobileprogrammation.com for more information.
